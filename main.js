const mainContentEl = document.querySelector('#main-content')

async function main() {
  const query = `
    query {
      products {
        id
        name
        price
      }
    }
  `
  const req = await fetch('http://localhost:8090/v1/graphql', {
    method: 'POST',
    body: JSON.stringify({ query }),
  })

  const res = await req.json()
  const { products } = res.data

  const makeListItem = (product) =>
    `<li>[${product.id}]: ${product.name} ($${product.price})</li>`

  const items = products.map(makeListItem).join('')
  if (mainContentEl) mainContentEl.innerHTML = items
}

main()
